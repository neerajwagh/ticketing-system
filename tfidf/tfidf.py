'''
	export PYTHONPATH=$PYTHONPATH:../lib
    /paththo/spark-submit
        --jars mongo-hadoop-spark-2.0.0-SNAPSHOT.jar
        --driver-class-path mongo-hadoop-spark-2.0.0-SNAPSHOT.jar
        --py-files pymongo_spark.py,
        dist/pymongo_spark-0.1.dev0-py2.7.egg tfidf.py
'''

import pymongo_spark
import re
import math
from pyspark import SparkContext,AccumulatorParam
from pymongo import MongoClient
import ConfigParser
import sys


def tf(item):
	'''
	Map document to vector space  using formula tf(t,d)=1+log10(f(t,d))
	'''
	global totalCountBr
	tC  = totalCountBr.value
	l1 = {}
	l = {}
	for it in item['Body']:
		h = hash(it) % tC
		l[h] = l.get(h,0)+1.0
	l = {str(k):str((1+math.log10(l[k]))) for k in l}
	l1[item['rid']] = l
	return l1


def convert_to_unit(item):
	'''
	Do cosine normalization of vector
	'''
	temp = 0.0
	for k in item:
		for j in item[k]:
			temp = temp + math.pow(float(item[k][j]),2)
	temp = float(math.sqrt(temp))
	for k in item:
		for j in item[k]:
			item[k][j] = str(float(item[k][j])/float(temp))
	return item


def inverseIndex(item):
	'''
	Extract the list for every word.
	'''
	l = []
	for k in item.values()[0]:
		l.append((k,item.keys()[0]))
	return l

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("Usage: tfidf.py <config_file> ")
		exit(-1)

	configParser = ConfigParser.RawConfigParser()
	configParser.read(sys.argv[1])
	hostname = configParser.get('mongodb','hostname')
	port = int(configParser.get('mongodb','port'))
	database = configParser.get('mongodb','database')
	urlDB = "mongodb://"+hostname+":"+str(port)+"/"+database
	
	pymongo_spark.activate()
	mc = MongoClient(hostname,27017)
	sc = SparkContext()
	
	#creating rdd from mongodb source
	rdd = sc.mongoRDD(urlDB+".fixed")

	#stop word broadcast variable.
	stopWord = open("../rsc/stop_words.txt").readlines()
	st = [i.strip() for i in stopWord ]
	br = sc.broadcast(st)
	
	#function to be applied for removing stop words from the body,tags,title
	def tokenize(item):
		"""
		Function to tokenize the bugs content
		"""
		textSources = [item['final_report']['op_sys'], item['final_report']['short_desc'], item['final_report']['product'], item['final_report']['component']]

		def aggregateTerms(text):
				return re.sub(r'\s+', ' ', re.sub(r'[^a-zA-Z]', ' ', text)).split()
        
		def getTermList(item):
				return [
					str(term).lower() for text in textSources for term in aggregateTerms(text)
					if term not in br.value
				]
		item['Body'] = getTermList(item)
		return item

	#func execution on every item to remove stop words and unneccessary operator.
	rddWTSt=rdd.map(tokenize)
	
	class JoinBody(AccumulatorParam):
		"""
		Sub class for accumulator
		"""
		def zero(self, value):
				return []
		
		def addInPlace(self, str1, str2):
			"""Logic for how to accumulate results"""
			str1 = str1 + str2
			return str1

	va=sc.accumulator([], JoinBody())

	def join_body(rItem):
		"""Collecting the words"""
		global va
		va += rItem['Body']

	rddWTSt.foreach(join_body)
	rddTotalWords = sc.parallelize(va.value).distinct()
	totalCount = rddTotalWords.count()
	totalCountBr = sc.broadcast(totalCount)
	totalc = {}
	totalc["TW"] = totalCount
	mc.eclipse.total_count.insert_one(totalc)
	mc.close()

	rddTf = rddWTSt.map(tf)
	rddUnitV = rddTf.map(convert_to_unit)
	rddUnitV.saveToMongoDB(urlDB+".unit_vector")

	rddInverIndex = rddTf.flatMap(inverseIndex).groupByKey().map(lambda x : (x[0], list(x[1])))
	rddInverIndex.saveToMongoDB(urlDB+".Inverted_index")
	
