'''
	/opt/spark_1.6.0/bin/spark-submit --jars /opt/spark_1.6.0/kafka_dependency/spark-streaming-kafka-assembly_2.11-1.6.0.jar,/opt/spark_1.6.0/kafka_dependency/scala-library-2.11.7.jar,mongo-hadoop-spark-2.0.0-SNAPSHOT.jar --driver-class-path mongo-hadoop-spark-2.0.0-SNAPSHOT.jar --py-files pymongo_spark.py,dist/pymongo_spark-0.1.dev0-py2.7.egg query.py
'''
import pymongo_spark
import re
import math
from pyspark import SparkContext,AccumulatorParam
from pyspark.mllib.linalg import Vectors
from pymongo import MongoClient
from pyspark.mllib.feature import HashingTF, IDF
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import NaiveBayes 
from pyspark import SparkContext  
sc=SparkContext()

pymongo_spark.activate()
mc=MongoClient("localhost",27017)
rdd=sc.parallelize(sc.mongoRDD("mongodb://localhost:27017/eclipse.bugs").take(2000))
complete=rdd.map(lambda doc: (doc["final_report"]["assigned_to"],doc["final_report"]["product"],doc["final_report"]["component"],doc["final_report"]["short_desc"])).repartition(5)
rem=complete.map(lambda doc: doc[0])
assig=sc.broadcast(rem.distinct().collect())
def index_assign(item):
	global assig
	return float(assig.value.index(item))
rem=rem.map(index_assign)
stopWord=open("scripts/rsc/stop_words.txt").readlines()
st=[i.strip() for i in stopWord ]
br=sc.broadcast(st)
def tf_cal(item):
		global br
		stV=br.value
		textSources = [item[1], item[2], item[3]]
		def aggregateTerms(text):
				return re.sub(r'\s+', ' ', re.sub(r'[^a-zA-Z]', ' ', text)).split()
		def getTermList(item):
				return [
					term for text in textSources for term in aggregateTerms(text)
					if term not in stV
					]
		return getTermList(item)
		
tf=HashingTF(numFeatures=41768).transform(complete.map(tf_cal, preservesPartitioning=True))
idf = IDF().fit(tf)
tfidf = idf.transform(tf)
complete_f=rem.zip(tfidf).map(lambda x: LabeledPoint(x[0], x[1]))
model = NaiveBayes.train(complete_f)
def func(item):
	if item[0] == float(item[1]):
		return item
labels_and_preds = rem.zip(model.predict(tfidf)).filter(func)
for k in labels_and_preds.take(10):
		print k


