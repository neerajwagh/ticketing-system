import pymongo
# from pyspark.mllib.regression import LabeledPoint

class DB:
    def __init__(self, db, host=None, port=None, **kwargs):
        self.db = pymongo.MongoClient(host=host, port=port)[db]
        self.developers = self.db.bugs.distinct('events.assigned_to')
        self.products = self.db.bugs.distinct('events.product')
        self.components = self.db.bugs.distinct('events.component')
        self.developersDict = {
          dev: index 
          for index, dev in enumerate(self.developers)
        }
        self.productsDict = {
          product: index 
          for index, product in enumerate(self.products)
        }
        self.componentsDict = {
          component: index 
          for index, component in enumerate(self.components)
        }

    # db.bugs.aggregate({$unwind: '$events'}, {$match: {'events.resolution': 'FIXED'}}, 
    #   {$sort: {'events.timestamp': 1}}, {$project:{'events': 1}})
    def getIterator(self, query):
        return self.db.bugs.aggregate(query)

def chronologicallyOrdered(bug):
    return sorted(bug[u'events'], key=lambda x: x[u'timestamp'])

def override(report, params):
    for key in params.keys():
        report[key] = params[key]
    return report

def finalReport(bug):
    events = chronologicallyOrdered(bug)
    report, otherEvents = events[0], events[1:]
    for event in otherEvents:
        report = override(report, event)
    return report

def getLabeledPoint(bug, developersDict, productsDict, componentsDict):
    report = finalReport(bug)
    return LabeledPoint(
       developersDict[report[u'assigned_to']],
       [productsDict[report[u'product']], componentsDict[report[u'component']]]
    )

