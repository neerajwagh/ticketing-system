'''
	export PYTHONPATH=$PYTHONPATH:../lib
    /paththo/spark-submit
        --jars /pathto/spark-streaming-kafka-assembly_2.11-1.6.0.jar,
        /pathto/scala-library-2.11.7.jar,
        mongo-hadoop-spark-2.0.0-SNAPSHOT.jar
        --driver-class-path mongo-hadoop-spark-2.0.0-SNAPSHOT.jar
        --py-files pymongo_spark.py,
        pymongo_spark-0.1.dev0-py2.7.egg query.py
'''

import pymongo_spark
from pyspark import SparkContext
import math
import re
import sys
import json
import ConfigParser
from collections import Counter
from pymongo import MongoClient
from pyspark.streaming import StreamingContext, StreamingListener
from pyspark.streaming.kafka import KafkaUtils
from pyspark.streaming.util import rddToFileName


class StreamListener(StreamingListener):
	"""
	Class for handling events on streaming.The query received 
	through streaming from apache kafka can not be used directly
	as spark streaming wrap them in batches.On batch submission 
	event we proceed with query normalization and cosine measure calculation.

	Listener takes spark context,normalized dataset.
	:param sc:
	SparkContext which is used to create streaming context.
	:param rddUnitVector:
	Normalized dataset.

	"""

	def __init__(self, sc, rddUnitVector,urlDFS):
		self.sc = sc
		self.rddUnitVector = rddUnitVector
		self.urlDFS = urlDFS

	def onBatchCompleted(self, batchCompleted):
		"""Function to handle batch completion event"""
		rd = self.sc.pickleFile(self.urlDFS+"/norm-" +
			str(batchCompleted.batchInfo().batchTime()).split(" ")[0])
		
		if rd.count() > 0:
			global b1
			b1 = self.sc.broadcast(rd.first())

			def cosine_measure(item):
				global b1
				v = b1.value
				j = {
					int(i):{
							int(m):float(item[i][m]) for m in item[i]
							} for i in item if i != "_id"
						}	
				li = filter(v[1].has_key,j.values()[0].keys())
				k = []
				if li:
			 		k.append(
			 			(j.keys()[0],reduce(
			 				(lambda x,y:x+y),map(
			 						(lambda x:v[1][x]*j.values()[0][x]),li
			 						)
			 					)
			 				)
			 			)
				else:
					pass
				return k
					
			r=rddUnitVector.flatMap(
				cosine_measure).takeOrdered(
				10,(lambda (key1,value1):-1*value1)
				)
			print r

	def onBatchStarted(self,batchStarted):
		pass
		
	def onBatchSubmitted(self,batchSubmitted):
		pass
		
	def onOutputOperationCompleted(self,outputOperationCompleted):
		pass
		
	def onOutputOperationStarted(self,outputOperationStarted):
		pass
		
	def onReceiverError(self,receiverError):
		pass
		
	def onReceiverStarted(self,receiverStarted):
		pass
		
	def onReceiverStopped(self,receiverStopped):
		pass


def total_doc():
	'''
	Returns total word count
	'''
	return mc.eclipse.fixed.count()

def get_stop_words():
	'''
	get stop word list
	'''
	stopWord = open(stop_words).readlines()
	st = [i.strip() for i in stopWord ]
	return st

def clean(item):
	'''
	Clean text passed as argument.Remove special symbol
	'''
	st = get_stop_words()
	l = []
	if item is not None:

		def aggregateTerms(text):
			return re.sub(r'\s+', ' ', re.sub(r'[^a-zA-Z]', ' ', text)).split()
		
		for k in aggregateTerms(item):
			if  str(k).lower() not in st and len(k)>1:
				l.append(str(k).lower())
	return l


def remove_stop_word(x):
	'''
	Clean and remove stop words from text
	'''
	l = []
	l = clean(x[1][0])+clean(x[1][1])+clean(x[1][2])
	return (x[0],l)


def hash_representation(vector_re):
	'''
	Calculate hash of attributes(words).
	'''
	global tCb
	c = Counter(vector_re[1])
	c = {(hash(k)%tCb.value):c[k] for k in c}
	return (vector_re[0],c)


def find_term(term):
	'''
	Find the inverted index for word.
	'''
	return mc.eclipse.Inverted_index.find_one({"_id":str(term)})

	
def calc_tfidf(hash_rep):
	'''
	TF*idf for the query.
	'''
	tc = total_doc()
	dic = {}
	for k in hash_rep[1]:
		d = find_term(k)
		if d:
			dic[k] = (1+math.log10(float(hash_rep[k])))*math.log10(1+float(tc)/float(len(d['value'])))	
	return (hash_rep[0],dic)


def normalization(tfidf):
	'''
	cosine normalization of the query vector.
	'''
	temp = 0.0
	for k in tfidf[1]:
		temp = temp+math.pow(math.log10(tfidf[1][k]),2)
	temp = math.sqrt(temp)
	for k in tfidf[1]:
		tfidf[1][k] = float(tfidf[1][k]/temp)
	return tfidf


if __name__=="__main__":
	if len(sys.argv) != 2:
		print("Usage: query.py <config_file> ")
		exit(-1)

	configParser = ConfigParser.RawConfigParser()
	configParser.read(sys.argv[1])
	hostname = configParser.get('mongodb','hostname')
	port = int(configParser.get('mongodb','port'))
	database = configParser.get('mongodb','database')
	urlDB = "mongodb://"+hostname+":"+str(port)+"/"+database

	zookeeper_hostname = configParser.get('kafka','zookeeper_hostname')
	zookeeper_port = int(configParser.get('kafka','zookeeper_port'))
	zookeeper=zookeeper_hostname+":"+str(zookeeper_port)
	topic = configParser.get('kafka','topic')

	namenode = configParser.get('hdfs','namenode')
	namenode_port = configParser.get('hdfs','namenode_port')
	location = configParser.get('hdfs','data_location')
	urlDFS= "hdfs://"+namenode+":"+namenode_port+"/"+location
	
	pymongo_spark.activate()
	#spark context creation
	sc = SparkContext()
	#Streaming context created	
	ssc = StreamingContext(sc, 1)

	mc = MongoClient(hostname,27017)
	#creating rdd from mongodb source.
	rddUnitVector = sc.mongoRDD(urlDB+".unit_vector")

	tC = int(mc.eclipse.total_count.find_one()["TW"])
	tCb = sc.broadcast(tC)
	#path to stop words will be replaced by other path in actual implementation
	stop_words = "scripts/rsc/stop_words.txt"

	mc.close()

	#kafka connection
	kvs = KafkaUtils.createStream(
		ssc, zookeeper, "streaming-consumer", {topic: 1})
	#Kafka streaming is activated
	#working with query now
	lines = kvs.map(lambda x: x[1])
	querytext = lines.map(
			lambda x:json.loads(x)).map(
				lambda x: x['payload']).map(
				lambda x:(
							x['rid'],
							[
								x['product'],
								x['component'],
								x['short_descr']
							]
						)
					)
	
	norm = querytext.map(
						remove_stop_word).map(
							hash_representation).map(
								normalization)

	def saveAsPickleFile(t, rdd):
		"""
		Saving DStream to hdfs so that it can be retrieved on batch 
		completion event
		"""
		path = rddToFileName(urlDFS+"/norm", None, t)
		rdd.saveAsPickleFile(path)

	norm.foreachRDD(saveAsPickleFile)
	
	ssc.addStreamingListener(
		StreamListener(
			sc,
			rddUnitVector,
			urlDFS
			)
		)

	ssc.start()
	ssc.awaitTermination()
    